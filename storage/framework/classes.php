<?php return array (
  'october\\demo\\Plugin' => 'plugins\\october\\demo\\Plugin.php',
  'backend\\Controllers\\index' => 'modules\\backend\\controllers\\index.php',
  'backend\\Controllers\\auth' => 'modules\\backend\\controllers\\auth.php',
  'backend\\Controllers\\preferences' => 'modules\\backend\\controllers\\preferences.php',
  'cms\\Controllers\\themes' => 'modules\\cms\\controllers\\themes.php',
  'system\\Controllers\\updates' => 'modules\\system\\controllers\\updates.php',
  'backend\\Controllers\\users' => 'modules\\backend\\controllers\\users.php',
  'system\\Controllers\\settings' => 'modules\\system\\controllers\\settings.php',
  'system\\Controllers\\mailtemplates' => 'modules\\system\\controllers\\mailtemplates.php',
  'cms\\Controllers\\index' => 'modules\\cms\\controllers\\index.php',
  'October\\Demo\\Components\\Todo' => 'plugins\\october\\demo\\components\\Todo.php',
  'rainlab\\pages\\Plugin' => 'plugins\\rainlab\\pages\\Plugin.php',
  'janvince\\smallcontactform\\Plugin' => 'plugins\\janvince\\smallcontactform\\Plugin.php',
  'rainlab\\translate\\Plugin' => 'plugins\\rainlab\\translate\\Plugin.php',
  'RainLab\\Translate\\Models\\Locale' => 'plugins\\rainlab\\translate\\models\\Locale.php',
  'RainLab\\Translate\\Models\\Attribute' => 'plugins\\rainlab\\translate\\models\\Attribute.php',
  'indikator\\news\\Plugin' => 'plugins\\indikator\\news\\Plugin.php',
  'rainlab\\builder\\Plugin' => 'plugins\\rainlab\\builder\\Plugin.php',
  'rainlab\\editable\\Plugin' => 'plugins\\rainlab\\editable\\Plugin.php',
  'Indikator\\News\\Controllers\\Posts' => 'plugins\\indikator\\news\\controllers\\Posts.php',
  'Indikator\\News\\Controllers\\Subscribers' => 'plugins\\indikator\\news\\controllers\\Subscribers.php',
  'RainLab\\Pages\\Classes\\SnippetManager' => 'plugins\\rainlab\\pages\\classes\\SnippetManager.php',
  'RainLab\\Translate\\Behaviors\\TranslatablePageUrl' => 'plugins\\rainlab\\translate\\behaviors\\TranslatablePageUrl.php',
  'RainLab\\Translate\\Classes\\Translator' => 'plugins\\rainlab\\translate\\classes\\Translator.php',
  'RainLab\\Translate\\Classes\\TranslatableBehavior' => 'plugins\\rainlab\\translate\\classes\\TranslatableBehavior.php',
  'RainLab\\Translate\\Behaviors\\TranslatablePage' => 'plugins\\rainlab\\translate\\behaviors\\TranslatablePage.php',
  'Indikator\\News\\Components\\Posts' => 'plugins\\indikator\\news\\components\\Posts.php',
  'Indikator\\News\\Components\\Post' => 'plugins\\indikator\\news\\components\\Post.php',
  'Indikator\\News\\Components\\Categories' => 'plugins\\indikator\\news\\components\\Categories.php',
  'Indikator\\News\\Classes\\SubscriberService' => 'plugins\\indikator\\news\\classes\\SubscriberService.php',
  'Indikator\\News\\Components\\Subscribe' => 'plugins\\indikator\\news\\components\\Subscribe.php',
  'Indikator\\News\\Components\\Unsubscribe' => 'plugins\\indikator\\news\\components\\Unsubscribe.php',
  'JanVince\\SmallContactForm\\Components\\SmallContactForm' => 'plugins\\janvince\\smallcontactform\\components\\SmallContactForm.php',
  'RainLab\\Builder\\Components\\RecordList' => 'plugins\\rainlab\\builder\\components\\RecordList.php',
  'RainLab\\Builder\\Components\\RecordDetails' => 'plugins\\rainlab\\builder\\components\\RecordDetails.php',
  'RainLab\\Editable\\Components\\Editable' => 'plugins\\rainlab\\editable\\components\\Editable.php',
  'RainLab\\Pages\\Components\\ChildPages' => 'plugins\\rainlab\\pages\\components\\ChildPages.php',
  'RainLab\\Pages\\Components\\StaticPage' => 'plugins\\rainlab\\pages\\components\\StaticPage.php',
  'RainLab\\Pages\\Components\\StaticMenu' => 'plugins\\rainlab\\pages\\components\\StaticMenu.php',
  'RainLab\\Pages\\Components\\StaticBreadcrumbs' => 'plugins\\rainlab\\pages\\components\\StaticBreadcrumbs.php',
  'RainLab\\Translate\\Components\\LocalePicker' => 'plugins\\rainlab\\translate\\components\\LocalePicker.php',
  'RainLab\\Translate\\Components\\AlternateHrefLangElements' => 'plugins\\rainlab\\translate\\components\\AlternateHrefLangElements.php',
  'RainLab\\Translate\\Behaviors\\TranslatableModel' => 'plugins\\rainlab\\translate\\behaviors\\TranslatableModel.php',
  'RainLab\\Translate\\Classes\\EventRegistry' => 'plugins\\rainlab\\translate\\classes\\EventRegistry.php',
  'RainLab\\Translate\\Traits\\MLControl' => 'plugins\\rainlab\\translate\\traits\\MLControl.php',
  'RainLab\\Translate\\FormWidgets\\MLText' => 'plugins\\rainlab\\translate\\formwidgets\\MLText.php',
  'RainLab\\Translate\\FormWidgets\\MLTextarea' => 'plugins\\rainlab\\translate\\formwidgets\\MLTextarea.php',
  'RainLab\\Pages\\Classes\\Snippet' => 'plugins\\rainlab\\pages\\classes\\Snippet.php',
  'RainLab\\Pages\\Classes\\Router' => 'plugins\\rainlab\\pages\\classes\\Router.php',
  'RainLab\\Pages\\Classes\\Page' => 'plugins\\rainlab\\pages\\classes\\Page.php',
  'RainLab\\Translate\\Classes\\LocaleMiddleware' => 'plugins\\rainlab\\translate\\classes\\LocaleMiddleware.php',
  'RainLab\\Pages\\Classes\\Controller' => 'plugins\\rainlab\\pages\\classes\\Controller.php',
  'RainLab\\Pages\\Classes\\PageList' => 'plugins\\rainlab\\pages\\classes\\PageList.php',
  'RainLab\\Translate\\Behaviors\\TranslatableCmsObject' => 'plugins\\rainlab\\translate\\behaviors\\TranslatableCmsObject.php',
  'RainLab\\Translate\\Models\\Message' => 'plugins\\rainlab\\translate\\models\\Message.php',
  'rainlab\\translate\\Controllers\\messages' => 'plugins\\rainlab\\translate\\controllers\\messages.php',
  'RainLab\\Translate\\Models\\MessageImport' => 'plugins\\rainlab\\translate\\models\\MessageImport.php',
  'RainLab\\Translate\\Models\\MessageExport' => 'plugins\\rainlab\\translate\\models\\MessageExport.php',
  'rainlab\\translate\\Controllers\\locales' => 'plugins\\rainlab\\translate\\controllers\\locales.php',
  'backend\\Controllers\\media' => 'modules\\backend\\controllers\\media.php',
);