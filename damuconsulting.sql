-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2021 at 03:39 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `damuconsulting`
--

-- --------------------------------------------------------

--
-- Table structure for table `backend_access_log`
--

CREATE TABLE `backend_access_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_access_log`
--

INSERT INTO `backend_access_log` (`id`, `user_id`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, 1, '::1', '2021-06-21 03:01:57', '2021-06-21 03:01:57');

-- --------------------------------------------------------

--
-- Table structure for table `backend_users`
--

CREATE TABLE `backend_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persist_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_activated` tinyint(1) NOT NULL DEFAULT 0,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_users`
--

INSERT INTO `backend_users` (`id`, `first_name`, `last_name`, `login`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `role_id`, `activated_at`, `last_login`, `created_at`, `updated_at`, `deleted_at`, `is_superuser`) VALUES
(1, 'Kurmangazy', 'Kongratbayev', 'admin', 'kurmangazykongratbayev@outlook.com', '$2y$10$z6vjBqogr1W9togcn1jXoOOqm401jZHg0hYzrMNnaK2n70cuOrxea', NULL, '$2y$10$ym6dxYeZbem7tiKjcFX9EuCVNFR40TLZm49PEQV0NiVed5tAdN6zu', NULL, '', 1, 2, NULL, '2021-06-21 03:01:57', '2021-06-21 02:56:41', '2021-06-21 03:01:57', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_users_groups`
--

CREATE TABLE `backend_users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_users_groups`
--

INSERT INTO `backend_users_groups` (`user_id`, `user_group_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_groups`
--

CREATE TABLE `backend_user_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_groups`
--

INSERT INTO `backend_user_groups` (`id`, `name`, `created_at`, `updated_at`, `code`, `description`, `is_new_user_default`) VALUES
(1, 'Owners', '2021-06-21 02:56:41', '2021-06-21 02:56:41', 'owners', 'Default group for website owners.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_preferences`
--

CREATE TABLE `backend_user_preferences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_preferences`
--

INSERT INTO `backend_user_preferences` (`id`, `user_id`, `namespace`, `group`, `item`, `value`) VALUES
(1, 1, 'backend', 'backend', 'preferences', '{\"locale\":\"en\",\"fallback_locale\":\"en\",\"timezone\":\"Europe\\/London\",\"editor_font_size\":\"12\",\"editor_word_wrap\":\"fluid\",\"editor_code_folding\":\"manual\",\"editor_tab_size\":\"4\",\"editor_theme\":\"twilight\",\"editor_show_invisibles\":\"0\",\"editor_highlight_active_line\":\"1\",\"editor_use_hard_tabs\":\"0\",\"editor_show_gutter\":\"1\",\"editor_auto_closing\":\"0\",\"editor_autocompletion\":\"manual\",\"editor_enable_snippets\":\"0\",\"editor_display_indent_guides\":\"0\",\"editor_show_print_margin\":\"0\",\"user_id\":1}');

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_roles`
--

CREATE TABLE `backend_user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_system` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_roles`
--

INSERT INTO `backend_user_roles` (`id`, `name`, `code`, `description`, `permissions`, `is_system`, `created_at`, `updated_at`) VALUES
(1, 'Publisher', 'publisher', 'Site editor with access to publishing tools.', '', 1, '2021-06-21 02:56:41', '2021-06-21 02:56:41'),
(2, 'Developer', 'developer', 'Site administrator with access to developer tools.', '', 1, '2021-06-21 02:56:41', '2021-06-21 02:56:41');

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_throttle`
--

CREATE TABLE `backend_user_throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT 0,
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT 0,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT 0,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_throttle`
--

INSERT INTO `backend_user_throttle` (`id`, `user_id`, `ip_address`, `attempts`, `last_attempt_at`, `is_suspended`, `suspended_at`, `is_banned`, `banned_at`) VALUES
(1, 1, '::1', 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE `cache` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_data`
--

CREATE TABLE `cms_theme_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_logs`
--

CREATE TABLE `cms_theme_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_templates`
--

CREATE TABLE `cms_theme_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `source` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(10) UNSIGNED NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `deferred_bindings`
--

CREATE TABLE `deferred_bindings` (
  `id` int(10) UNSIGNED NOT NULL,
  `master_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master_field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `failed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `indikator_news_categories`
--

CREATE TABLE `indikator_news_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hidden` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2',
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `indikator_news_newsletter_logs`
--

CREATE TABLE `indikator_news_newsletter_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `news_id` int(10) UNSIGNED DEFAULT NULL,
  `subscriber_id` int(10) UNSIGNED DEFAULT NULL,
  `queued_at` datetime NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `viewed_at` datetime DEFAULT NULL,
  `clicked_at` datetime DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_id` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `indikator_news_posts`
--

CREATE TABLE `indikator_news_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `introductory` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `statistics` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` smallint(6) NOT NULL DEFAULT 2,
  `last_send_at` datetime DEFAULT NULL,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `enable_newsletter_content` tinyint(1) NOT NULL DEFAULT 0,
  `newsletter_content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `tags` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_desc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `indikator_news_relations`
--

CREATE TABLE `indikator_news_relations` (
  `subscriber_id` int(10) UNSIGNED NOT NULL,
  `categories_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `indikator_news_subscribers`
--

CREATE TABLE `indikator_news_subscribers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `statistics` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `locale` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registered_at` datetime DEFAULT NULL,
  `registered_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `confirmed_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmation_hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unsubscribed_at` datetime DEFAULT NULL,
  `unsubscribed_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `janvince_smallcontactform_messages`
--

CREATE TABLE `janvince_smallcontactform_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_message` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remote_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_10_01_000001_Db_Deferred_Bindings', 1),
(2, '2013_10_01_000002_Db_System_Files', 1),
(3, '2013_10_01_000003_Db_System_Plugin_Versions', 1),
(4, '2013_10_01_000004_Db_System_Plugin_History', 1),
(5, '2013_10_01_000005_Db_System_Settings', 1),
(6, '2013_10_01_000006_Db_System_Parameters', 1),
(7, '2013_10_01_000007_Db_System_Add_Disabled_Flag', 1),
(8, '2013_10_01_000008_Db_System_Mail_Templates', 1),
(9, '2013_10_01_000009_Db_System_Mail_Layouts', 1),
(10, '2014_10_01_000010_Db_Jobs', 1),
(11, '2014_10_01_000011_Db_System_Event_Logs', 1),
(12, '2014_10_01_000012_Db_System_Request_Logs', 1),
(13, '2014_10_01_000013_Db_System_Sessions', 1),
(14, '2015_10_01_000014_Db_System_Mail_Layout_Rename', 1),
(15, '2015_10_01_000015_Db_System_Add_Frozen_Flag', 1),
(16, '2015_10_01_000016_Db_Cache', 1),
(17, '2015_10_01_000017_Db_System_Revisions', 1),
(18, '2015_10_01_000018_Db_FailedJobs', 1),
(19, '2016_10_01_000019_Db_System_Plugin_History_Detail_Text', 1),
(20, '2016_10_01_000020_Db_System_Timestamp_Fix', 1),
(21, '2017_08_04_121309_Db_Deferred_Bindings_Add_Index_Session', 1),
(22, '2017_10_01_000021_Db_System_Sessions_Update', 1),
(23, '2017_10_01_000022_Db_Jobs_FailedJobs_Update', 1),
(24, '2017_10_01_000023_Db_System_Mail_Partials', 1),
(25, '2017_10_23_000024_Db_System_Mail_Layouts_Add_Options_Field', 1),
(26, '2013_10_01_000001_Db_Backend_Users', 2),
(27, '2013_10_01_000002_Db_Backend_User_Groups', 2),
(28, '2013_10_01_000003_Db_Backend_Users_Groups', 2),
(29, '2013_10_01_000004_Db_Backend_User_Throttle', 2),
(30, '2014_01_04_000005_Db_Backend_User_Preferences', 2),
(31, '2014_10_01_000006_Db_Backend_Access_Log', 2),
(32, '2014_10_01_000007_Db_Backend_Add_Description_Field', 2),
(33, '2015_10_01_000008_Db_Backend_Add_Superuser_Flag', 2),
(34, '2016_10_01_000009_Db_Backend_Timestamp_Fix', 2),
(35, '2017_10_01_000010_Db_Backend_User_Roles', 2),
(36, '2018_12_16_000011_Db_Backend_Add_Deleted_At', 2),
(37, '2014_10_01_000001_Db_Cms_Theme_Data', 3),
(38, '2016_10_01_000002_Db_Cms_Timestamp_Fix', 3),
(39, '2017_10_01_000003_Db_Cms_Theme_Logs', 3),
(40, '2018_11_01_000001_Db_Cms_Theme_Templates', 3);

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_attributes`
--

CREATE TABLE `rainlab_translate_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_data` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_indexes`
--

CREATE TABLE `rainlab_translate_indexes` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_locales`
--

CREATE TABLE `rainlab_translate_locales` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  `is_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `sort_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rainlab_translate_locales`
--

INSERT INTO `rainlab_translate_locales` (`id`, `code`, `name`, `is_default`, `is_enabled`, `sort_order`) VALUES
(1, 'en', 'English', 1, 1, 1),
(2, 'ru', 'Русский', 0, 1, 2),
(3, 'kz', 'Қазақша', 0, 1, 3),
(4, 'tr', 'Türkçe', 0, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_messages`
--

CREATE TABLE `rainlab_translate_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_data` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `found` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rainlab_translate_messages`
--

INSERT INTO `rainlab_translate_messages` (`id`, `code`, `message_data`, `found`) VALUES
(1, 'company.name', '{\"x\":\"Company name\"}', 1),
(2, 'company.description', '{\"x\":\"Company description\"}', 1),
(3, 'facebook.link', '{\"x\":\"facebook link\"}', 1),
(4, 'instagram.link', '{\"x\":\"instagram link\"}', 1),
(5, 'whatsapp.link', '{\"x\":\"whatsapp link\"}', 1),
(6, 'useful.links', '{\"x\":\"Useful Links\"}', 1),
(7, 'home.page', '{\"x\":\"Home page\"}', 1),
(8, 'our.services', '{\"x\":\"Our services\"}', 1),
(9, 'about.us', '{\"x\":\"About us\"}', 1),
(10, 'pricing', '{\"x\":\"Pricing\"}', 1),
(11, 'news', '{\"x\":\"News\"}', 1),
(12, 'contact.us', '{\"x\":\"Contact us\"}', 1),
(13, 'recent.news', '{\"x\":\"Recent News\"}', 1),
(14, 'general.information', '{\"x\":\"General information\"}', 1),
(15, 'about.company', '{\"x\":\"About company\"}', 1),
(16, 'success.stories', '{\"x\":\"Success stories\"}', 1),
(17, 'contact.phone', '{\"x\":\"Contact phone\"}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_activity` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_event_logs`
--

CREATE TABLE `system_event_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_files`
--

CREATE TABLE `system_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `disk_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT 1,
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_files`
--

INSERT INTO `system_files` (`id`, `disk_name`, `file_name`, `file_size`, `content_type`, `title`, `description`, `field`, `attachment_id`, `attachment_type`, `is_public`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, '60d056914e369240136872.png', 'logo-dark.png', 30138, 'image/png', NULL, NULL, 'logo', '1', 'Backend\\Models\\BrandSetting', 1, 1, '2021-06-21 03:06:25', '2021-06-21 03:06:29'),
(2, '60d05693318d8680068729.png', 'logo-dark.png', 30138, 'image/png', NULL, NULL, 'favicon', '1', 'Backend\\Models\\BrandSetting', 1, 2, '2021-06-21 03:06:27', '2021-06-21 03:06:29');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_layouts`
--

CREATE TABLE `system_mail_layouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_css` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_locked` tinyint(1) NOT NULL DEFAULT 0,
  `options` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_mail_layouts`
--

INSERT INTO `system_mail_layouts` (`id`, `name`, `code`, `content_html`, `content_text`, `content_css`, `is_locked`, `options`, `created_at`, `updated_at`) VALUES
(1, 'Default layout', 'default', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-default\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n\n        <!-- Header -->\n        {% partial \'header\' body %}\n            {{ subject|raw }}\n        {% endpartial %}\n\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n\n        <!-- Footer -->\n        {% partial \'footer\' body %}\n            &copy; {{ \"now\"|date(\"Y\") }} {{ appName }}. All rights reserved.\n        {% endpartial %}\n\n    </table>\n\n</body>\n</html>', '{{ content|raw }}', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, NULL, '2021-06-21 02:56:41', '2021-06-21 02:56:41'),
(2, 'System layout', 'system', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-system\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n\n                                        <!-- Subcopy -->\n                                        {% partial \'subcopy\' body %}\n                                            **This is an automatic message. Please do not reply to it.**\n                                        {% endpartial %}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n    </table>\n\n</body>\n</html>', '{{ content|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, NULL, '2021-06-21 02:56:41', '2021-06-21 02:56:41');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_partials`
--

CREATE TABLE `system_mail_partials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_mail_partials`
--

INSERT INTO `system_mail_partials` (`id`, `name`, `code`, `content_html`, `content_text`, `is_custom`, `created_at`, `updated_at`) VALUES
(1, 'Header', 'header', '<tr>\n    <td class=\"header\">\n        {% if url %}\n            <a href=\"{{ url }}\">\n                {{ body }}\n            </a>\n        {% else %}\n            <span>\n                {{ body }}\n            </span>\n        {% endif %}\n    </td>\n</tr>', '*** {{ body|trim }} <{{ url }}>', 0, '2021-06-21 03:07:06', '2021-06-21 03:07:06'),
(2, 'Footer', 'footer', '<tr>\n    <td>\n        <table class=\"footer\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n            <tr>\n                <td class=\"content-cell\" align=\"center\">\n                    {{ body|md_safe }}\n                </td>\n            </tr>\n        </table>\n    </td>\n</tr>', '-------------------\n{{ body|trim }}', 0, '2021-06-21 03:07:06', '2021-06-21 03:07:06'),
(3, 'Button', 'button', '<table class=\"action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td align=\"center\">\n            <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                <tr>\n                    <td align=\"center\">\n                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                            <tr>\n                                <td>\n                                    <a href=\"{{ url }}\" class=\"button button-{{ type ?: \'primary\' }}\" target=\"_blank\">\n                                        {{ body }}\n                                    </a>\n                                </td>\n                            </tr>\n                        </table>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>', '{{ body|trim }} <{{ url }}>', 0, '2021-06-21 03:07:06', '2021-06-21 03:07:06'),
(4, 'Panel', 'panel', '<table class=\"panel break-all\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td class=\"panel-content\">\n            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                <tr>\n                    <td class=\"panel-item\">\n                        {{ body|md_safe }}\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>', '{{ body|trim }}', 0, '2021-06-21 03:07:06', '2021-06-21 03:07:06'),
(5, 'Table', 'table', '<div class=\"table\">\n    {{ body|md_safe }}\n</div>', '{{ body|trim }}', 0, '2021-06-21 03:07:06', '2021-06-21 03:07:06'),
(6, 'Subcopy', 'subcopy', '<table class=\"subcopy\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td>\n            {{ body|md_safe }}\n        </td>\n    </tr>\n</table>', '-----\n{{ body|trim }}', 0, '2021-06-21 03:07:06', '2021-06-21 03:07:06'),
(7, 'Promotion', 'promotion', '<table class=\"promotion break-all\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td align=\"center\">\n            {{ body|md_safe }}\n        </td>\n    </tr>\n</table>', '{{ body|trim }}', 0, '2021-06-21 03:07:06', '2021-06-21 03:07:06');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_templates`
--

CREATE TABLE `system_mail_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `layout_id` int(11) DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_mail_templates`
--

INSERT INTO `system_mail_templates` (`id`, `code`, `subject`, `description`, `content_html`, `content_text`, `layout_id`, `is_custom`, `created_at`, `updated_at`) VALUES
(1, 'backend::mail.invite', NULL, 'Invite new admin to the site', NULL, NULL, 2, 0, '2021-06-21 03:07:06', '2021-06-21 03:07:06'),
(2, 'backend::mail.restore', NULL, 'Reset an admin password', NULL, NULL, 2, 0, '2021-06-21 03:07:06', '2021-06-21 03:07:06');

-- --------------------------------------------------------

--
-- Table structure for table `system_parameters`
--

CREATE TABLE `system_parameters` (
  `id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_parameters`
--

INSERT INTO `system_parameters` (`id`, `namespace`, `group`, `item`, `value`) VALUES
(1, 'system', 'update', 'count', '0'),
(2, 'system', 'core', 'hash', '\"652ebd026445ccaeb3ba083f00830dc0\"'),
(3, 'system', 'core', 'build', '\"473\"'),
(4, 'system', 'update', 'retry', '1624352519'),
(5, 'cms', 'theme', 'active', '\"damu\"');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_history`
--

CREATE TABLE `system_plugin_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_plugin_history`
--

INSERT INTO `system_plugin_history` (`id`, `code`, `type`, `version`, `detail`, `created_at`) VALUES
(1, 'October.Demo', 'comment', '1.0.1', 'First version of Demo', '2021-06-21 02:56:41'),
(2, 'RainLab.Pages', 'comment', '1.0.1', 'Implemented the static pages management and the Static Page component.', '2021-06-21 03:16:04'),
(3, 'RainLab.Pages', 'comment', '1.0.2', 'Fixed the page preview URL.', '2021-06-21 03:16:04'),
(4, 'RainLab.Pages', 'comment', '1.0.3', 'Implemented menus.', '2021-06-21 03:16:04'),
(5, 'RainLab.Pages', 'comment', '1.0.4', 'Implemented the content block management and placeholder support.', '2021-06-21 03:16:04'),
(6, 'RainLab.Pages', 'comment', '1.0.5', 'Added support for the Sitemap plugin.', '2021-06-21 03:16:04'),
(7, 'RainLab.Pages', 'comment', '1.0.6', 'Minor updates to the internal API.', '2021-06-21 03:16:04'),
(8, 'RainLab.Pages', 'comment', '1.0.7', 'Added the Snippets feature.', '2021-06-21 03:16:04'),
(9, 'RainLab.Pages', 'comment', '1.0.8', 'Minor improvements to the code.', '2021-06-21 03:16:04'),
(10, 'RainLab.Pages', 'comment', '1.0.9', 'Fixes issue where Snippet tab is missing from the Partials form.', '2021-06-21 03:16:04'),
(11, 'RainLab.Pages', 'comment', '1.0.10', 'Add translations for various locales.', '2021-06-21 03:16:04'),
(12, 'RainLab.Pages', 'comment', '1.0.11', 'Fixes issue where placeholders tabs were missing from Page form.', '2021-06-21 03:16:04'),
(13, 'RainLab.Pages', 'comment', '1.0.12', 'Implement Media Manager support.', '2021-06-21 03:16:04'),
(14, 'RainLab.Pages', 'script', '1.1.0', 'snippets_rename_viewbag_properties.php', '2021-06-21 03:16:04'),
(15, 'RainLab.Pages', 'comment', '1.1.0', 'Adds meta title and description to pages. Adds |staticPage filter.', '2021-06-21 03:16:04'),
(16, 'RainLab.Pages', 'comment', '1.1.1', 'Add support for Syntax Fields.', '2021-06-21 03:16:04'),
(17, 'RainLab.Pages', 'comment', '1.1.2', 'Static Breadcrumbs component now respects the hide from navigation setting.', '2021-06-21 03:16:04'),
(18, 'RainLab.Pages', 'comment', '1.1.3', 'Minor back-end styling fix.', '2021-06-21 03:16:04'),
(19, 'RainLab.Pages', 'comment', '1.1.4', 'Minor fix to the StaticPage component API.', '2021-06-21 03:16:04'),
(20, 'RainLab.Pages', 'comment', '1.1.5', 'Fixes bug when using syntax fields.', '2021-06-21 03:16:04'),
(21, 'RainLab.Pages', 'comment', '1.1.6', 'Minor styling fix to the back-end UI.', '2021-06-21 03:16:04'),
(22, 'RainLab.Pages', 'comment', '1.1.7', 'Improved menu item form to include CSS class, open in a new window and hidden flag.', '2021-06-21 03:16:04'),
(23, 'RainLab.Pages', 'comment', '1.1.8', 'Improved the output of snippet partials when saved.', '2021-06-21 03:16:04'),
(24, 'RainLab.Pages', 'comment', '1.1.9', 'Minor update to snippet inspector internal API.', '2021-06-21 03:16:04'),
(25, 'RainLab.Pages', 'comment', '1.1.10', 'Fixes a bug where selecting a layout causes permanent unsaved changes.', '2021-06-21 03:16:04'),
(26, 'RainLab.Pages', 'comment', '1.1.11', 'Add support for repeater syntax field.', '2021-06-21 03:16:04'),
(27, 'RainLab.Pages', 'comment', '1.2.0', 'Added support for translations, UI updates.', '2021-06-21 03:16:04'),
(28, 'RainLab.Pages', 'comment', '1.2.1', 'Use nice titles when listing the content files.', '2021-06-21 03:16:04'),
(29, 'RainLab.Pages', 'comment', '1.2.2', 'Minor styling update.', '2021-06-21 03:16:04'),
(30, 'RainLab.Pages', 'comment', '1.2.3', 'Snippets can now be moved by dragging them.', '2021-06-21 03:16:04'),
(31, 'RainLab.Pages', 'comment', '1.2.4', 'Fixes a bug where the cursor is misplaced when editing text files.', '2021-06-21 03:16:04'),
(32, 'RainLab.Pages', 'comment', '1.2.5', 'Fixes a bug where the parent page is lost upon changing a page layout.', '2021-06-21 03:16:04'),
(33, 'RainLab.Pages', 'comment', '1.2.6', 'Shared view variables are now passed to static pages.', '2021-06-21 03:16:04'),
(34, 'RainLab.Pages', 'comment', '1.2.7', 'Fixes issue with duplicating properties when adding multiple snippets on the same page.', '2021-06-21 03:16:04'),
(35, 'RainLab.Pages', 'comment', '1.2.8', 'Fixes a bug where creating a content block without extension doesn\'t save the contents to file.', '2021-06-21 03:16:04'),
(36, 'RainLab.Pages', 'comment', '1.2.9', 'Add conditional support for translating page URLs.', '2021-06-21 03:16:04'),
(37, 'RainLab.Pages', 'comment', '1.2.10', 'Streamline generation of URLs to use the new Cms::url helper.', '2021-06-21 03:16:04'),
(38, 'RainLab.Pages', 'comment', '1.2.11', 'Implements repeater usage with translate plugin.', '2021-06-21 03:16:04'),
(39, 'RainLab.Pages', 'comment', '1.2.12', 'Fixes minor issue when using snippets and switching the application locale.', '2021-06-21 03:16:04'),
(40, 'RainLab.Pages', 'comment', '1.2.13', 'Fixes bug when AJAX is used on a page that does not yet exist.', '2021-06-21 03:16:04'),
(41, 'RainLab.Pages', 'comment', '1.2.14', 'Add theme logging support for changes made to menus.', '2021-06-21 03:16:04'),
(42, 'RainLab.Pages', 'comment', '1.2.15', 'Back-end navigation sort order updated.', '2021-06-21 03:16:04'),
(43, 'RainLab.Pages', 'comment', '1.2.16', 'Fixes a bug when saving a template that has been modified outside of the CMS (mtime mismatch).', '2021-06-21 03:16:04'),
(44, 'RainLab.Pages', 'comment', '1.2.17', 'Changes locations of custom fields to secondary tabs instead of the primary Settings area. New menu search ability on adding menu items', '2021-06-21 03:16:04'),
(45, 'RainLab.Pages', 'comment', '1.2.18', 'Fixes cache-invalidation issues when RainLab.Translate is not installed. Added Greek & Simplified Chinese translations. Removed deprecated calls. Allowed saving HTML in snippet properties. Added support for the MediaFinder in menu items.', '2021-06-21 03:16:04'),
(46, 'RainLab.Pages', 'comment', '1.2.19', 'Catch exception with corrupted menu file.', '2021-06-21 03:16:04'),
(47, 'RainLab.Pages', 'comment', '1.2.20', 'StaticMenu component now exposes menuName property; added pages.menu.referencesGenerated event.', '2021-06-21 03:16:04'),
(48, 'RainLab.Pages', 'comment', '1.2.21', 'Fixes a bug where last Static Menu item cannot be deleted. Improved Persian, Slovak and Turkish translations.', '2021-06-21 03:16:04'),
(49, 'RainLab.Pages', 'comment', '1.3.0', 'Added support for using Database-driven Themes when enabled in the CMS configuration.', '2021-06-21 03:16:04'),
(50, 'RainLab.Pages', 'comment', '1.3.1', 'Added ChildPages Component, prevent hidden pages from being returned via menu item resolver.', '2021-06-21 03:16:04'),
(51, 'RainLab.Pages', 'comment', '1.3.2', 'Fixes error when creating a subpage whose parent has no layout set.', '2021-06-21 03:16:04'),
(52, 'RainLab.Pages', 'comment', '1.3.3', 'Improves user experience for users with only partial access through permissions', '2021-06-21 03:16:04'),
(53, 'RainLab.Pages', 'comment', '1.3.4', 'Fix error where large menus were being truncated due to the PHP \"max_input_vars\" configuration value. Improved Slovenian translation.', '2021-06-21 03:16:04'),
(54, 'RainLab.Pages', 'comment', '1.3.5', 'Minor fix to bust the browser cache for JS assets. Prevent duplicate property fields in snippet inspector.', '2021-06-21 03:16:04'),
(55, 'RainLab.Pages', 'comment', '1.3.6', 'ChildPages component now displays localized page titles from Translate plugin.', '2021-06-21 03:16:04'),
(56, 'RainLab.Pages', 'comment', '1.3.7', 'Adds MenuPicker formwidget. Adds future support for v2.0 of October CMS.', '2021-06-21 03:16:04'),
(57, 'RainLab.Pages', 'comment', '1.4.0', 'Fixes bug when adding menu items in October CMS v2.0.', '2021-06-21 03:16:04'),
(58, 'RainLab.Pages', 'comment', '1.4.1', 'Fixes support for configuration values.', '2021-06-21 03:16:04'),
(59, 'RainLab.Pages', 'comment', '1.4.3', 'Fixes page deletion is newer platform builds.', '2021-06-21 03:16:04'),
(60, 'RainLab.Pages', 'comment', '1.4.4', 'Disable touch device detection', '2021-06-21 03:16:04'),
(61, 'RainLab.Pages', 'comment', '1.4.5', 'Minor styling improvements', '2021-06-21 03:16:04'),
(62, 'JanVince.SmallContactForm', 'script', '1.0.0', 'scf_tables.php', '2021-06-21 03:19:47'),
(63, 'JanVince.SmallContactForm', 'comment', '1.0.0', 'First version of Small Contact Form plugin', '2021-06-21 03:19:47'),
(64, 'JanVince.SmallContactForm', 'comment', '1.0.1', 'Fix form hiding after successful send', '2021-06-21 03:19:47'),
(65, 'JanVince.SmallContactForm', 'comment', '1.0.1', 'Fix in README.md', '2021-06-21 03:19:47'),
(66, 'JanVince.SmallContactForm', 'comment', '1.0.2', 'Fix some typos and add LICENCE file (thanks Szabó Gergő)', '2021-06-21 03:19:47'),
(67, 'JanVince.SmallContactForm', 'comment', '1.1.0', 'Added function to delete records in Messages list', '2021-06-21 03:19:47'),
(68, 'JanVince.SmallContactForm', 'comment', '1.1.0', 'Added permission to delete records', '2021-06-21 03:19:47'),
(69, 'JanVince.SmallContactForm', 'comment', '1.2.0', 'Added dashboard report widgets (Stats and New messages)', '2021-06-21 03:19:47'),
(70, 'JanVince.SmallContactForm', 'comment', '1.2.1', 'Mail templates now render values with {{ values|raw }}', '2021-06-21 03:19:47'),
(71, 'JanVince.SmallContactForm', 'comment', '1.2.2', 'Mail templates convert new lines to <br> with {{ values|raw|nl2br }}', '2021-06-21 03:19:47'),
(72, 'JanVince.SmallContactForm', 'comment', '1.2.3', 'Fields mapping moved to separate tab *Columns mapping*', '2021-06-21 03:19:47'),
(73, 'JanVince.SmallContactForm', 'comment', '1.2.4', 'Updated README.md with assets usage example', '2021-06-21 03:19:47'),
(74, 'JanVince.SmallContactForm', 'script', '1.2.5', 'scf_tables_02.php', '2021-06-21 03:19:47'),
(75, 'JanVince.SmallContactForm', 'comment', '1.2.5', 'Added IP protection function (limit too many submits from one IP address)', '2021-06-21 03:19:47'),
(76, 'JanVince.SmallContactForm', 'comment', '1.2.5', 'And Messages list column to show senders IP address (invisible by default)', '2021-06-21 03:19:47'),
(77, 'JanVince.SmallContactForm', 'comment', '1.2.6', 'Fixed IP protection error message', '2021-06-21 03:19:47'),
(78, 'JanVince.SmallContactForm', 'comment', '1.2.7', 'Changed remote_ip column type to string', '2021-06-21 03:19:47'),
(79, 'JanVince.SmallContactForm', 'comment', '1.2.8', 'Added option to use placeholders instead of labels', '2021-06-21 03:19:47'),
(80, 'JanVince.SmallContactForm', 'comment', '1.3.0', 'Added translation support for Rainlab Translate plugin', '2021-06-21 03:19:47'),
(81, 'JanVince.SmallContactForm', 'comment', '1.3.0', 'Fixed some typos', '2021-06-21 03:19:47'),
(82, 'JanVince.SmallContactForm', 'comment', '1.3.1', 'Added default value for getTranslated() method', '2021-06-21 03:19:47'),
(83, 'JanVince.SmallContactForm', 'comment', '1.3.2', 'Added custom send button wrapper class', '2021-06-21 03:19:47'),
(84, 'JanVince.SmallContactForm', 'comment', '1.4.0', 'Added redirect option after successful submit (internal and external URL)', '2021-06-21 03:19:47'),
(85, 'JanVince.SmallContactForm', 'comment', '1.4.1', 'Minor UI fix (thanks Szabó Gergő)', '2021-06-21 03:19:47'),
(86, 'JanVince.SmallContactForm', 'comment', '1.4.2', 'Added support for default translated mail templates (Czech and English for now)', '2021-06-21 03:19:47'),
(87, 'JanVince.SmallContactForm', 'comment', '1.4.3', 'Fixed translation of mail templates description in Settings > Mail templates', '2021-06-21 03:19:47'),
(88, 'JanVince.SmallContactForm', 'comment', '1.4.4', 'Fixed array of enabledLocales', '2021-06-21 03:19:47'),
(89, 'JanVince.SmallContactForm', 'comment', '1.4.5', 'Fixed email template check', '2021-06-21 03:19:47'),
(90, 'JanVince.SmallContactForm', 'comment', '1.4.5', 'Added default EN locale to enabled locales array', '2021-06-21 03:19:47'),
(91, 'JanVince.SmallContactForm', 'comment', '1.4.6', 'Removed field type restriction for Fields mapping', '2021-06-21 03:19:47'),
(92, 'JanVince.SmallContactForm', 'comment', '1.4.7', 'Removed hardcoded date format for created_at column in messages list, updated README and added hungarian language (thanks Szabó Gergő for all this)', '2021-06-21 03:19:47'),
(93, 'JanVince.SmallContactForm', 'comment', '1.4.8', 'Changes to allow multiple use of contact form (form and message blocks has now unique IDs)', '2021-06-21 03:19:47'),
(94, 'JanVince.SmallContactForm', 'comment', '1.4.8', 'Added checkbox field type', '2021-06-21 03:19:47'),
(95, 'JanVince.SmallContactForm', 'comment', '1.4.8', 'Scoreboard last message time format (thanks Szabó Gergő)', '2021-06-21 03:19:47'),
(96, 'JanVince.SmallContactForm', 'comment', '1.4.9', 'Added scoreboard button to quickly open form settings', '2021-06-21 03:19:47'),
(97, 'JanVince.SmallContactForm', 'comment', '1.4.10', 'Fixed typo in lang filename', '2021-06-21 03:19:47'),
(98, 'JanVince.SmallContactForm', 'comment', '1.4.11', 'Added \"fieldsDetails\" array to all email templates to have access to field labels, types and more', '2021-06-21 03:19:47'),
(99, 'JanVince.SmallContactForm', 'comment', '1.4.11', 'Updated default autoreply mail templates to include fieldsDetail array', '2021-06-21 03:19:47'),
(100, 'JanVince.SmallContactForm', 'comment', '1.4.11', 'Added function to detect non-defined fields in sent data', '2021-06-21 03:19:47'),
(101, 'JanVince.SmallContactForm', 'comment', '1.4.11', 'Updated README.md file', '2021-06-21 03:19:47'),
(102, 'JanVince.SmallContactForm', 'comment', '1.5.0', 'Added some component hacking options (override autoreply and notification emails and template, disable fields)', '2021-06-21 03:19:47'),
(103, 'JanVince.SmallContactForm', 'comment', '1.5.0', 'Fixed some typos', '2021-06-21 03:19:47'),
(104, 'JanVince.SmallContactForm', 'comment', '1.5.0', 'Updated README.md file', '2021-06-21 03:19:47'),
(105, 'JanVince.SmallContactForm', 'comment', '1.5.1', 'Fixed flash message visibility when where are some errors', '2021-06-21 03:19:47'),
(106, 'JanVince.SmallContactForm', 'comment', '1.5.2', 'Fixed flash error for IP protection visibility', '2021-06-21 03:19:47'),
(107, 'JanVince.SmallContactForm', 'comment', '1.5.3', 'Added option for notification emails to have FROM address set from contact form email field', '2021-06-21 03:19:47'),
(108, 'JanVince.SmallContactForm', 'comment', '1.5.4', 'Added option to mark selected messages as read', '2021-06-21 03:19:47'),
(109, 'JanVince.SmallContactForm', 'comment', '1.5.5', 'Changed JSON type for repeater DB column', '2021-06-21 03:19:47'),
(110, 'JanVince.SmallContactForm', 'comment', '1.5.6', 'Removed value attribute in textarea field', '2021-06-21 03:19:47'),
(111, 'JanVince.SmallContactForm', 'comment', '1.5.7', 'Added component alias to id attributes for multi-form usage', '2021-06-21 03:19:47'),
(112, 'JanVince.SmallContactForm', 'comment', '1.5.8', 'Fixed typo in lang files', '2021-06-21 03:19:47'),
(113, 'JanVince.SmallContactForm', 'comment', '1.5.9', 'Added direct link to messages list from dashboard widget', '2021-06-21 03:19:47'),
(114, 'JanVince.SmallContactForm', 'comment', '1.6.0', 'Added Google reCAPTCHA validation', '2021-06-21 03:19:47'),
(115, 'JanVince.SmallContactForm', 'comment', '1.6.1', 'Changed All messages large indicator to New messages in scoreboard', '2021-06-21 03:19:47'),
(116, 'JanVince.SmallContactForm', 'comment', '1.6.2', 'Removed reCAPTCHA hard coded locale string (thx kuzyk). Added settings option to allow locale detection.', '2021-06-21 03:19:47'),
(117, 'JanVince.SmallContactForm', 'comment', '1.7.0', 'Added option to specify <label> custom CSS class', '2021-06-21 03:19:47'),
(118, 'JanVince.SmallContactForm', 'comment', '1.7.1', 'Fixed \'text_preview\' list field type truncate function (thx kuzyk)', '2021-06-21 03:19:47'),
(119, 'JanVince.SmallContactForm', 'comment', '1.7.2', 'Changed count() to mb_strlen() function in custom list type definition', '2021-06-21 03:19:47'),
(120, 'JanVince.SmallContactForm', 'comment', '1.8.0', 'Added option to disable built-in browser form validation. Added class \'is-invalid\' for fields with error (as used Bootstrap 4).', '2021-06-21 03:19:47'),
(121, 'JanVince.SmallContactForm', 'comment', '1.9.0', 'Form registered as Page snippet to be used in Rainlab.Page content (thx BtzLeon)', '2021-06-21 03:19:47'),
(122, 'JanVince.SmallContactForm', 'comment', '1.9.1', 'REPLY TO address is now used for notification email by default. You can still force FROM address to be used (but this is not supported by all email systems!).', '2021-06-21 03:19:47'),
(123, 'JanVince.SmallContactForm', 'comment', '1.9.2', 'Fix problem when ReCaptcha field was logged as an undefined field to system log (thx LukeTowers)', '2021-06-21 03:19:47'),
(124, 'JanVince.SmallContactForm', 'comment', '1.9.3', 'Fixed label \'for\' attribute to point to input ID (as required by specification)', '2021-06-21 03:19:47'),
(125, 'JanVince.SmallContactForm', 'comment', '1.10.0', 'Added form component hacks group (now only for disabling notification emails, more will come)', '2021-06-21 03:19:47'),
(126, 'JanVince.SmallContactForm', 'script', '1.11.0', 'scf_tables_03.php', '2021-06-21 03:19:47'),
(127, 'JanVince.SmallContactForm', 'comment', '1.11.0', 'Added form fields alias and description (can be used to distinquish between more forms or to save extra data). More info in README file.', '2021-06-21 03:19:47'),
(128, 'JanVince.SmallContactForm', 'comment', '1.11.1', 'Added form description field to message preview', '2021-06-21 03:19:47'),
(129, 'JanVince.SmallContactForm', 'comment', '1.12.0', 'Added Russian translation (thank Dinver)', '2021-06-21 03:19:47'),
(130, 'JanVince.SmallContactForm', 'comment', '1.12.1', 'Chanded input custom list column type for switch to prevent interaction with toolbar JS', '2021-06-21 03:19:47'),
(131, 'JanVince.SmallContactForm', 'comment', '1.13.1', 'Added form_alias and form_description variables to email (notification and autoreply) templates', '2021-06-21 03:19:47'),
(132, 'JanVince.SmallContactForm', 'comment', '1.13.2', 'Disabled placeholder attribute for checkbox', '2021-06-21 03:19:47'),
(133, 'JanVince.SmallContactForm', 'comment', '1.14.0', 'Added option to export messages list', '2021-06-21 03:19:47'),
(134, 'JanVince.SmallContactForm', 'comment', '1.14.1', 'Added permissions to export messages list', '2021-06-21 03:19:47'),
(135, 'JanVince.SmallContactForm', 'comment', '1.15.0', 'Added Privacy tab and new option to disable sent messages saving', '2021-06-21 03:19:47'),
(136, 'JanVince.SmallContactForm', 'comment', '1.15.1', 'Fixed settings fields trigger', '2021-06-21 03:19:47'),
(137, 'JanVince.SmallContactForm', 'comment', '1.15.2', 'Fixed default values for recaptcha settings to false', '2021-06-21 03:19:47'),
(138, 'JanVince.SmallContactForm', 'comment', '1.15.3', 'Allowed combination of disabled messages saving and allowed passive antispam', '2021-06-21 03:19:47'),
(139, 'JanVince.SmallContactForm', 'comment', '1.16.0', 'Added option to have more than one notification email address', '2021-06-21 03:19:47'),
(140, 'JanVince.SmallContactForm', 'comment', '1.16.1', 'Fixed missing form data in autoreply templates. Updated default autoreply messages.', '2021-06-21 03:19:47'),
(141, 'JanVince.SmallContactForm', 'comment', '1.16.2', 'Updated hungarian translation (thx gergo85)', '2021-06-21 03:19:47'),
(142, 'JanVince.SmallContactForm', 'comment', '1.16.3', 'Fixed checkbox validation and validation state', '2021-06-21 03:19:47'),
(143, 'JanVince.SmallContactForm', 'comment', '1.17.0', 'Added Slovak translation (thx vosco88)', '2021-06-21 03:19:47'),
(144, 'JanVince.SmallContactForm', 'comment', '1.18.0', 'Added French translations (thx FelixINX)', '2021-06-21 03:19:47'),
(145, 'JanVince.SmallContactForm', 'comment', '1.19.0', 'Added custom validation fields (thanks petr-vytlacil for help)', '2021-06-21 03:19:47'),
(146, 'JanVince.SmallContactForm', 'comment', '1.20.0', 'Added dropdown field type', '2021-06-21 03:19:47'),
(147, 'JanVince.SmallContactForm', 'comment', '1.21.0', 'Form fields repeater is now translatable', '2021-06-21 03:19:47'),
(148, 'JanVince.SmallContactForm', 'comment', '1.22.0', 'Fixed multiple flash messages shown', '2021-06-21 03:19:47'),
(149, 'JanVince.SmallContactForm', 'comment', '1.23.0', 'When placeholders are used, labels are now still present and only hidden by style attribute', '2021-06-21 03:19:47'),
(150, 'JanVince.SmallContactForm', 'comment', '1.24.0', 'Added option to set custom reCaptcha wrapper CSS class', '2021-06-21 03:19:47'),
(151, 'JanVince.SmallContactForm', 'comment', '1.25.0', 'Added polish (thanks Magiczne) and spanish (thanks codibit) translations', '2021-06-21 03:19:47'),
(152, 'JanVince.SmallContactForm', 'comment', '1.30.0', 'Added invisible reCaptcha', '2021-06-21 03:19:47'),
(153, 'JanVince.SmallContactForm', 'comment', '1.30.1', 'Fixed reCaptcha scripts load', '2021-06-21 03:19:47'),
(154, 'JanVince.SmallContactForm', 'comment', '1.31.2', 'Fixed AJAX redirect when validation error (thanks zlobec)', '2021-06-21 03:19:47'),
(155, 'JanVince.SmallContactForm', 'comment', '1.31.3', 'Fixed reCaptcha checkbox version not showing up on older installations', '2021-06-21 03:19:47'),
(156, 'JanVince.SmallContactForm', 'comment', '1.31.4', 'Fixed unnecessary refresh after Ajax send (thanks cregx)', '2021-06-21 03:19:47'),
(157, 'JanVince.SmallContactForm', 'comment', '1.32.0', 'Added all settings overrides as regular component properties (can be used to override some form settings in multi-form setup)', '2021-06-21 03:19:47'),
(158, 'JanVince.SmallContactForm', 'comment', '1.32.0', 'Updated documentation', '2021-06-21 03:19:47'),
(159, 'JanVince.SmallContactForm', 'comment', '1.32.1', 'Fixed test on empty values if some fields are disabled', '2021-06-21 03:19:47'),
(160, 'JanVince.SmallContactForm', 'comment', '1.40.0', 'Added validation rule custom_not_regex (inverse of default regex validation rule)', '2021-06-21 03:19:47'),
(161, 'JanVince.SmallContactForm', 'comment', '1.40.0', 'Added form container with ID and action attribute with this hash URL (to automatically jump to form after nonAJAX send or refresh)', '2021-06-21 03:19:47'),
(162, 'JanVince.SmallContactForm', 'comment', '1.40.1', 'Fixed notification From name to be correctly set from component properties (thanks @pavsid)', '2021-06-21 03:19:47'),
(163, 'JanVince.SmallContactForm', 'comment', '1.41.0', 'Added component redirect properties and allow dynamic redirect URL as a component markup parameter. More info in README file.', '2021-06-21 03:19:47'),
(164, 'JanVince.SmallContactForm', 'comment', '1.41.0', 'Removed hard coded form hash URL (#scf-[form-alias]) as this can be now easily added with redirect options.', '2021-06-21 03:19:47'),
(165, 'JanVince.SmallContactForm', 'comment', '1.41.1', 'Set redirect URL property default value to null.', '2021-06-21 03:19:47'),
(166, 'JanVince.SmallContactForm', 'comment', '1.42.0', 'Added Google Analytics events after form is successfully sent', '2021-06-21 03:19:47'),
(167, 'JanVince.SmallContactForm', 'comment', '1.42.0', 'Do not populate redirection field code when redirection is not allowed', '2021-06-21 03:19:47'),
(168, 'JanVince.SmallContactForm', 'comment', '1.43.0', '!!! Rewritten component partials. No action is needed if you use plugin as is. But if you override component partials test them before update!', '2021-06-21 03:19:47'),
(169, 'JanVince.SmallContactForm', 'comment', '1.44.0', 'Added component properties to override notification and autoreply email subject (with support for Twig variables)', '2021-06-21 03:19:47'),
(170, 'JanVince.SmallContactForm', 'comment', '1.45.0', 'Added German translation (thanks NiklasDah)', '2021-06-21 03:19:47'),
(171, 'JanVince.SmallContactForm', 'comment', '1.46.0', 'Fixed backend validation for reCaptcha invisible', '2021-06-21 03:19:47'),
(172, 'JanVince.SmallContactForm', 'comment', '1.47.0', 'Added Custom code and Custom content field types', '2021-06-21 03:19:47'),
(173, 'JanVince.SmallContactForm', 'comment', '1.47.0', 'Updated README', '2021-06-21 03:19:47'),
(174, 'JanVince.SmallContactForm', 'comment', '1.47.1', 'Fixed typo in README (wrong component redirection parameter name)', '2021-06-21 03:19:48'),
(175, 'JanVince.SmallContactForm', 'comment', '1.47.2', 'Fixed checkbox validation (thanks Chocofede)', '2021-06-21 03:19:48'),
(176, 'JanVince.SmallContactForm', 'comment', '1.47.3', 'Removed unnecessarry name attribute from custom_content field', '2021-06-21 03:19:48'),
(177, 'JanVince.SmallContactForm', 'comment', '1.47.4', 'Fixed typo in field attributes', '2021-06-21 03:19:48'),
(178, 'JanVince.SmallContactForm', 'comment', '1.48.0', 'Added option to set ReplyTo email address for autoreply emails', '2021-06-21 03:19:48'),
(179, 'JanVince.SmallContactForm', 'comment', '1.48.1', 'Fixed autoreply email addresses checks', '2021-06-21 03:19:48'),
(180, 'JanVince.SmallContactForm', 'comment', '1.48.2', 'Fixed empty ReplyTo field error', '2021-06-21 03:19:48'),
(181, 'JanVince.SmallContactForm', 'comment', '1.48.3', 'Fixed getFieldHtmlCode method (thanks sdlb)', '2021-06-21 03:19:48'),
(182, 'JanVince.SmallContactForm', 'comment', '1.49.0', 'Fixed missing description and redirect url after AJAX calls', '2021-06-21 03:19:48'),
(183, 'JanVince.SmallContactForm', 'comment', '1.49.0', 'Fields forms in backend are now collapsed by default for better visibility', '2021-06-21 03:19:48'),
(184, 'JanVince.SmallContactForm', 'script', '1.50.0', 'scf_tables_04.php', '2021-06-21 03:19:48'),
(185, 'JanVince.SmallContactForm', 'comment', '1.50.0', 'Auto store form request URL in DB (is available also in Mail templates)', '2021-06-21 03:19:48'),
(186, 'JanVince.SmallContactForm', 'comment', '1.50.1', 'Removed unnecessarry debug log', '2021-06-21 03:19:48'),
(187, 'JanVince.SmallContactForm', 'comment', '1.51.0', 'Added support for (one or many) file uploads', '2021-06-21 03:19:48'),
(188, 'JanVince.SmallContactForm', 'comment', '1.51.0', 'Fixed AJAX validation', '2021-06-21 03:19:48'),
(189, 'JanVince.SmallContactForm', 'comment', '1.51.0', 'Updated README', '2021-06-21 03:19:48'),
(190, 'JanVince.SmallContactForm', 'comment', '1.51.1', 'Removed uploads array from default fields sent to autoreply template', '2021-06-21 03:19:48'),
(191, 'JanVince.SmallContactForm', 'comment', '1.51.2', 'Fixed uploads field in email messages', '2021-06-21 03:19:48'),
(192, 'JanVince.SmallContactForm', 'script', '1.51.3', 'scf_tables_05.php', '2021-06-21 03:19:48'),
(193, 'JanVince.SmallContactForm', 'comment', '1.51.3', 'Changed size of database column url (thanks zlobec)', '2021-06-21 03:19:48'),
(194, 'JanVince.SmallContactForm', 'comment', '1.51.4', 'Fixed passive antispam delay validation', '2021-06-21 03:19:48'),
(195, 'JanVince.SmallContactForm', 'comment', '1.52.0', 'Changed reCaptcha validation to work with allow_url_fopen disabled', '2021-06-21 03:19:48'),
(196, 'JanVince.SmallContactForm', 'comment', '1.52.1', 'Fixed project git files', '2021-06-21 03:19:48'),
(197, 'RainLab.Translate', 'script', '1.0.1', 'create_messages_table.php', '2021-06-21 03:22:07'),
(198, 'RainLab.Translate', 'script', '1.0.1', 'create_attributes_table.php', '2021-06-21 03:22:07'),
(199, 'RainLab.Translate', 'script', '1.0.1', 'create_locales_table.php', '2021-06-21 03:22:07'),
(200, 'RainLab.Translate', 'comment', '1.0.1', 'First version of Translate', '2021-06-21 03:22:07'),
(201, 'RainLab.Translate', 'comment', '1.0.2', 'Languages and Messages can now be deleted.', '2021-06-21 03:22:07'),
(202, 'RainLab.Translate', 'comment', '1.0.3', 'Minor updates for latest October release.', '2021-06-21 03:22:07'),
(203, 'RainLab.Translate', 'comment', '1.0.4', 'Locale cache will clear when updating a language.', '2021-06-21 03:22:07'),
(204, 'RainLab.Translate', 'comment', '1.0.5', 'Add Spanish language and fix plugin config.', '2021-06-21 03:22:07'),
(205, 'RainLab.Translate', 'comment', '1.0.6', 'Minor improvements to the code.', '2021-06-21 03:22:07'),
(206, 'RainLab.Translate', 'comment', '1.0.7', 'Fixes major bug where translations are skipped entirely!', '2021-06-21 03:22:07'),
(207, 'RainLab.Translate', 'comment', '1.0.8', 'Minor bug fixes.', '2021-06-21 03:22:07'),
(208, 'RainLab.Translate', 'comment', '1.0.9', 'Fixes an issue where newly created models lose their translated values.', '2021-06-21 03:22:07'),
(209, 'RainLab.Translate', 'comment', '1.0.10', 'Minor fix for latest build.', '2021-06-21 03:22:07'),
(210, 'RainLab.Translate', 'comment', '1.0.11', 'Fix multilingual rich editor when used in stretch mode.', '2021-06-21 03:22:07'),
(211, 'RainLab.Translate', 'comment', '1.1.0', 'Introduce compatibility with RainLab.Pages plugin.', '2021-06-21 03:22:07'),
(212, 'RainLab.Translate', 'comment', '1.1.1', 'Minor UI fix to the language picker.', '2021-06-21 03:22:07'),
(213, 'RainLab.Translate', 'comment', '1.1.2', 'Add support for translating Static Content files.', '2021-06-21 03:22:07'),
(214, 'RainLab.Translate', 'comment', '1.1.3', 'Improved support for the multilingual rich editor.', '2021-06-21 03:22:07'),
(215, 'RainLab.Translate', 'comment', '1.1.4', 'Adds new multilingual markdown editor.', '2021-06-21 03:22:07'),
(216, 'RainLab.Translate', 'comment', '1.1.5', 'Minor update to the multilingual control API.', '2021-06-21 03:22:07'),
(217, 'RainLab.Translate', 'comment', '1.1.6', 'Minor improvements in the message editor.', '2021-06-21 03:22:07'),
(218, 'RainLab.Translate', 'comment', '1.1.7', 'Fixes bug not showing content when first loading multilingual textarea controls.', '2021-06-21 03:22:07'),
(219, 'RainLab.Translate', 'comment', '1.2.0', 'CMS pages now support translating the URL.', '2021-06-21 03:22:07'),
(220, 'RainLab.Translate', 'comment', '1.2.1', 'Minor update in the rich editor and code editor language control position.', '2021-06-21 03:22:07'),
(221, 'RainLab.Translate', 'comment', '1.2.2', 'Static Pages now support translating the URL.', '2021-06-21 03:22:07'),
(222, 'RainLab.Translate', 'comment', '1.2.3', 'Fixes Rich Editor when inserting a page link.', '2021-06-21 03:22:07'),
(223, 'RainLab.Translate', 'script', '1.2.4', 'create_indexes_table.php', '2021-06-21 03:22:07'),
(224, 'RainLab.Translate', 'comment', '1.2.4', 'Translatable attributes can now be declared as indexes.', '2021-06-21 03:22:07'),
(225, 'RainLab.Translate', 'comment', '1.2.5', 'Adds new multilingual repeater form widget.', '2021-06-21 03:22:07'),
(226, 'RainLab.Translate', 'comment', '1.2.6', 'Fixes repeater usage with static pages plugin.', '2021-06-21 03:22:07'),
(227, 'RainLab.Translate', 'comment', '1.2.7', 'Fixes placeholder usage with static pages plugin.', '2021-06-21 03:22:07'),
(228, 'RainLab.Translate', 'comment', '1.2.8', 'Improvements to code for latest October build compatibility.', '2021-06-21 03:22:07'),
(229, 'RainLab.Translate', 'comment', '1.2.9', 'Fixes context for translated strings when used with Static Pages.', '2021-06-21 03:22:07'),
(230, 'RainLab.Translate', 'comment', '1.2.10', 'Minor UI fix to the multilingual repeater.', '2021-06-21 03:22:07'),
(231, 'RainLab.Translate', 'comment', '1.2.11', 'Fixes translation not working with partials loaded via AJAX.', '2021-06-21 03:22:07'),
(232, 'RainLab.Translate', 'comment', '1.2.12', 'Add support for translating the new grouped repeater feature.', '2021-06-21 03:22:07'),
(233, 'RainLab.Translate', 'comment', '1.3.0', 'Added search to the translate messages page.', '2021-06-21 03:22:07'),
(234, 'RainLab.Translate', 'script', '1.3.1', 'builder_table_update_rainlab_translate_locales.php', '2021-06-21 03:22:07'),
(235, 'RainLab.Translate', 'script', '1.3.1', 'seed_all_tables.php', '2021-06-21 03:22:07'),
(236, 'RainLab.Translate', 'comment', '1.3.1', 'Added reordering to languages', '2021-06-21 03:22:07'),
(237, 'RainLab.Translate', 'comment', '1.3.2', 'Improved compatibility with RainLab.Pages, added ability to scan Mail Messages for translatable variables.', '2021-06-21 03:22:07'),
(238, 'RainLab.Translate', 'comment', '1.3.3', 'Fix to the locale picker session handling in Build 420 onwards.', '2021-06-21 03:22:07'),
(239, 'RainLab.Translate', 'comment', '1.3.4', 'Add alternate hreflang elements and adds prefixDefaultLocale setting.', '2021-06-21 03:22:07'),
(240, 'RainLab.Translate', 'comment', '1.3.5', 'Fix MLRepeater bug when switching locales.', '2021-06-21 03:22:07'),
(241, 'RainLab.Translate', 'comment', '1.3.6', 'Fix Middleware to use the prefixDefaultLocale setting introduced in 1.3.4', '2021-06-21 03:22:07'),
(242, 'RainLab.Translate', 'comment', '1.3.7', 'Fix config reference in LocaleMiddleware', '2021-06-21 03:22:07'),
(243, 'RainLab.Translate', 'comment', '1.3.8', 'Keep query string when switching locales', '2021-06-21 03:22:07'),
(244, 'RainLab.Translate', 'comment', '1.4.0', 'Add importer and exporter for messages', '2021-06-21 03:22:07'),
(245, 'RainLab.Translate', 'comment', '1.4.1', 'Updated Hungarian translation. Added Arabic translation. Fixed issue where default texts are overwritten by import. Fixed issue where the language switcher for repeater fields would overlap with the first repeater row.', '2021-06-21 03:22:07'),
(246, 'RainLab.Translate', 'comment', '1.4.2', 'Add multilingual MediaFinder', '2021-06-21 03:22:07'),
(247, 'RainLab.Translate', 'comment', '1.4.3', '!!! Please update OctoberCMS to Build 444 before updating this plugin. Added ability to translate CMS Pages fields (e.g. title, description, meta-title, meta-description)', '2021-06-21 03:22:07'),
(248, 'RainLab.Translate', 'comment', '1.4.4', 'Minor improvements to compatibility with Laravel framework.', '2021-06-21 03:22:07'),
(249, 'RainLab.Translate', 'comment', '1.4.5', 'Fixed issue when using the language switcher', '2021-06-21 03:22:07'),
(250, 'RainLab.Translate', 'comment', '1.5.0', 'Compatibility fix with Build 451', '2021-06-21 03:22:07'),
(251, 'RainLab.Translate', 'comment', '1.6.0', 'Make File Upload widget properties translatable. Merge Repeater core changes into MLRepeater widget. Add getter method to retrieve original translate data.', '2021-06-21 03:22:07'),
(252, 'RainLab.Translate', 'comment', '1.6.1', 'Add ability for models to provide translated computed data, add option to disable locale prefix routing', '2021-06-21 03:22:07'),
(253, 'RainLab.Translate', 'comment', '1.6.2', 'Implement localeUrl filter, add per-locale theme configuration support', '2021-06-21 03:22:07'),
(254, 'RainLab.Translate', 'comment', '1.6.3', 'Add eager loading for translations, restore support for accessors & mutators', '2021-06-21 03:22:07'),
(255, 'RainLab.Translate', 'comment', '1.6.4', 'Fixes PHP 7.4 compatibility', '2021-06-21 03:22:07'),
(256, 'RainLab.Translate', 'comment', '1.6.5', 'Fixes compatibility issue when other plugins use a custom model morph map', '2021-06-21 03:22:07'),
(257, 'RainLab.Translate', 'script', '1.6.6', 'migrate_morphed_attributes.php', '2021-06-21 03:22:07'),
(258, 'RainLab.Translate', 'comment', '1.6.6', 'Introduce migration to patch existing translations using morph map', '2021-06-21 03:22:07'),
(259, 'RainLab.Translate', 'script', '1.6.7', 'migrate_morphed_indexes.php', '2021-06-21 03:22:07'),
(260, 'RainLab.Translate', 'comment', '1.6.7', 'Introduce migration to patch existing indexes using morph map', '2021-06-21 03:22:07'),
(261, 'RainLab.Translate', 'comment', '1.6.8', 'Add support for transOrderBy; Add translation support for ThemeData; Update russian localization.', '2021-06-21 03:22:07'),
(262, 'RainLab.Translate', 'comment', '1.6.9', 'Clear Static Page menu cache after saving the model; CSS fix for Text/Textarea input fields language selector.', '2021-06-21 03:22:07'),
(263, 'RainLab.Translate', 'script', '1.6.10', 'update_messages_table.php', '2021-06-21 03:22:07'),
(264, 'RainLab.Translate', 'comment', '1.6.10', 'Add option to purge deleted messages when scanning messages, Add Scan error column on Messages page, Fix translations that were lost when clicking locale twice while holding ctrl key, Fix error with nested fields default locale value, Escape Message translate params value.', '2021-06-21 03:22:07'),
(265, 'RainLab.Translate', 'comment', '1.7.0', '!!! Breaking change for the Message::trans() method (params are now escaped), fix message translation documentation, fix string translation key for scan errors column header.', '2021-06-21 03:22:07'),
(266, 'RainLab.Translate', 'comment', '1.7.1', 'Fix YAML issue with previous tag/release.', '2021-06-21 03:22:07'),
(267, 'RainLab.Translate', 'comment', '1.7.2', 'Fix regex when \"|_\" filter is followed by another filter, Try locale without country before returning default translation, Allow exporting default locale, Fire \'rainlab.translate.themeScanner.afterScan\' event in the theme scanner for extendability.', '2021-06-21 03:22:07'),
(268, 'RainLab.Translate', 'comment', '1.7.3', 'Make plugin ready for Laravel 6 update, Add support for translating RainLab.Pages MenuItem properties (requires RainLab.Pages v1.3.6), Restore multilingual button position for textarea, Fix translatableAttributes.', '2021-06-21 03:22:07'),
(269, 'RainLab.Translate', 'comment', '1.7.4', 'Faster version of transWhere, Mail templates/views can now be localized, Fix messages table layout on mobile, Fix scopeTransOrderBy duplicates, Polish localization updates, Turkish localization updates, Add Greek language localization.', '2021-06-21 03:22:07'),
(270, 'RainLab.Translate', 'comment', '1.8.0', 'Adds initial support for October v2.0', '2021-06-21 03:22:07'),
(271, 'RainLab.Translate', 'comment', '1.8.1', 'Minor bugfix', '2021-06-21 03:22:07'),
(272, 'RainLab.Translate', 'comment', '1.8.2', 'Fixes translated file models and theme data for v2.0. The parent model must implement translatable behavior for their related file models to be translated.', '2021-06-21 03:22:07'),
(273, 'RainLab.Translate', 'comment', '1.8.4', 'Fixes the multilingual mediafinder to work with the media module.', '2021-06-21 03:22:07'),
(274, 'RainLab.Translate', 'comment', '1.8.6', 'Fixes invisible checkboxes when scanning for messages.', '2021-06-21 03:22:07'),
(275, 'RainLab.Translate', 'comment', '1.8.7', 'Fixes Markdown editor translation.', '2021-06-21 03:22:07'),
(276, 'RainLab.Translate', 'comment', '1.8.8', 'Fixes Laravel compatibility in custom Repeater.', '2021-06-21 03:22:07'),
(277, 'RainLab.Translate', 'comment', '1.9.0', 'Restores ability to translate URLs with CMS Editor in October v2.0', '2021-06-21 03:22:07'),
(278, 'RainLab.Translate', 'comment', '1.9.1', 'Minor styling improvements', '2021-06-21 03:22:07'),
(279, 'Indikator.News', 'script', '1.0.0', 'create_posts_table.php', '2021-06-21 03:22:29'),
(280, 'Indikator.News', 'script', '1.0.0', 'create_subscribers_table.php', '2021-06-21 03:22:29'),
(281, 'Indikator.News', 'comment', '1.0.0', 'First version of News & Newsletter.', '2021-06-21 03:22:29'),
(282, 'Indikator.News', 'comment', '1.0.1', 'Modified the subject of emails.', '2021-06-21 03:22:29'),
(283, 'Indikator.News', 'comment', '1.1.0', 'Added two front-end components.', '2021-06-21 03:22:29'),
(284, 'Indikator.News', 'comment', '1.1.1', 'Improved the permissions.', '2021-06-21 03:22:29'),
(285, 'Indikator.News', 'comment', '1.1.2', 'Added the Published at field.', '2021-06-21 03:22:29'),
(286, 'Indikator.News', 'comment', '1.1.3', 'Minor improvements and bugfix.', '2021-06-21 03:22:29'),
(287, 'Indikator.News', 'comment', '1.2.0', 'Added post front-end components.', '2021-06-21 03:22:29'),
(288, 'Indikator.News', 'comment', '1.2.1', 'Added the Introductory field.', '2021-06-21 03:22:29'),
(289, 'Indikator.News', 'comment', '1.2.2', 'Improvement the post status.', '2021-06-21 03:22:29'),
(290, 'Indikator.News', 'script', '1.2.3', 'add_image_field_to_table.php', '2021-06-21 03:22:29'),
(291, 'Indikator.News', 'comment', '1.2.3', 'Select images via Media finder.', '2021-06-21 03:22:29'),
(292, 'Indikator.News', 'comment', '1.2.4', 'Minor improvements.', '2021-06-21 03:22:29'),
(293, 'Indikator.News', 'comment', '1.2.5', 'Support the Translate plugin.', '2021-06-21 03:22:29'),
(294, 'Indikator.News', 'comment', '1.2.6', 'Improved the posts component.', '2021-06-21 03:22:29'),
(295, 'Indikator.News', 'comment', '1.2.7', 'Added Russian translation.', '2021-06-21 03:22:29'),
(296, 'Indikator.News', 'comment', '1.2.8', 'Fixed the translate issues.', '2021-06-21 03:22:29'),
(297, 'Indikator.News', 'comment', '1.3.0', 'Improvements and bug fixes.', '2021-06-21 03:22:29'),
(298, 'Indikator.News', 'comment', '1.3.1', 'Fixed the posts component issue.', '2021-06-21 03:22:29'),
(299, 'Indikator.News', 'comment', '1.3.2', 'Improved the widget exception handling.', '2021-06-21 03:22:29'),
(300, 'Indikator.News', 'comment', '1.3.3', 'Improved the dashboard posts widget.', '2021-06-21 03:22:29'),
(301, 'Indikator.News', 'comment', '1.3.4', 'Improved the post form and list.', '2021-06-21 03:22:29'),
(302, 'Indikator.News', 'comment', '1.3.5', 'Minor improvements.', '2021-06-21 03:22:29'),
(303, 'Indikator.News', 'comment', '1.3.6', 'Added Polish translation.', '2021-06-21 03:22:29'),
(304, 'Indikator.News', 'comment', '1.3.7', 'Improvements and bug fixes.', '2021-06-21 03:22:29'),
(305, 'Indikator.News', 'comment', '1.3.8', 'Autocomplete the Slug field.', '2021-06-21 03:22:29'),
(306, 'Indikator.News', 'comment', '1.3.9', 'Minor improvements.', '2021-06-21 03:22:29'),
(307, 'Indikator.News', 'comment', '1.4.0', 'Fully compatible for Translate plugin.', '2021-06-21 03:22:29'),
(308, 'Indikator.News', 'comment', '1.4.1', 'Support the Sitemap plugin.', '2021-06-21 03:22:29'),
(309, 'Indikator.News', 'comment', '1.4.2', 'Minor UI improvements.', '2021-06-21 03:22:29'),
(310, 'Indikator.News', 'comment', '1.4.3', 'Added the daterange filters for lists.', '2021-06-21 03:22:29'),
(311, 'Indikator.News', 'comment', '1.4.4', 'Added the image column to post list.', '2021-06-21 03:22:29'),
(312, 'Indikator.News', 'script', '1.4.5', 'add_new_fields_to_table.php', '2021-06-21 03:22:29'),
(313, 'Indikator.News', 'script', '1.4.5', 'update_timestamp_nullable.php', '2021-06-21 03:22:29'),
(314, 'Indikator.News', 'comment', '1.4.5', 'Minor improvements and bugfix.', '2021-06-21 03:22:29'),
(315, 'Indikator.News', 'comment', '1.4.6', 'Fixed the post view issue.', '2021-06-21 03:22:29'),
(316, 'Indikator.News', 'comment', '1.4.7', 'Added the Statistics page.', '2021-06-21 03:22:29'),
(317, 'Indikator.News', 'comment', '1.4.8', 'Added more post statistics.', '2021-06-21 03:22:29'),
(318, 'Indikator.News', 'comment', '1.5.0', 'Added translation support for slug.', '2021-06-21 03:22:29'),
(319, 'Indikator.News', 'comment', '1.5.1', 'Improved the front-end components.', '2021-06-21 03:22:29'),
(320, 'Indikator.News', 'comment', '1.5.2', 'SEO support for front-end component.', '2021-06-21 03:22:29'),
(321, 'Indikator.News', 'comment', '1.5.3', 'Added 404 support to component.', '2021-06-21 03:22:29'),
(322, 'Indikator.News', 'comment', '1.5.4', 'Minor improvements.', '2021-06-21 03:22:29'),
(323, 'Indikator.News', 'comment', '1.5.5', 'Support the SiteSearch plugin.', '2021-06-21 03:22:29'),
(324, 'Indikator.News', 'comment', '1.5.6', 'Added the Import/Export feature.', '2021-06-21 03:22:29'),
(325, 'Indikator.News', 'script', '1.5.7', 'change_columns_type.php', '2021-06-21 03:22:29'),
(326, 'Indikator.News', 'comment', '1.5.7', 'Fixed the statistics columns sorting.', '2021-06-21 03:22:29'),
(327, 'Indikator.News', 'comment', '1.5.8', 'Added featured filtering on PostList.', '2021-06-21 03:22:29'),
(328, 'Indikator.News', 'comment', '1.6.0', 'Added the sending test email feature.', '2021-06-21 03:22:29'),
(329, 'Indikator.News', 'comment', '1.6.1', 'Added the Length column for Post list.', '2021-06-21 03:22:29'),
(330, 'Indikator.News', 'comment', '1.6.2', 'Added new icon for main navigation.', '2021-06-21 03:22:29'),
(331, 'Indikator.News', 'comment', '1.7.0', 'Added unsubscribe feature.', '2021-06-21 03:22:29'),
(332, 'Indikator.News', 'comment', '1.7.1', 'Added Deutsch translation.', '2021-06-21 03:22:29'),
(333, 'Indikator.News', 'comment', '1.7.2', 'Translated the front-end components.', '2021-06-21 03:22:29'),
(334, 'Indikator.News', 'comment', '1.7.3', 'Post view statistics now automatic.', '2021-06-21 03:22:29'),
(335, 'Indikator.News', 'script', '1.7.4', 'add_locale_field_to_table.php', '2021-06-21 03:22:29'),
(336, 'Indikator.News', 'comment', '1.7.4', 'Added the multilingual subscription.', '2021-06-21 03:22:29'),
(337, 'Indikator.News', 'comment', '1.7.5', 'Redesigned the report widgets.', '2021-06-21 03:22:29'),
(338, 'Indikator.News', 'comment', '1.7.6', 'Added two brand new report widgets.', '2021-06-21 03:22:29'),
(339, 'Indikator.News', 'comment', '1.7.7', 'Added Featured column and minor bugfix.', '2021-06-21 03:22:29'),
(340, 'Indikator.News', 'comment', '1.7.8', 'Improved the list of Subscribers.', '2021-06-21 03:22:29'),
(341, 'Indikator.News', 'script', '1.8.0', 'create_logs_table.php', '2021-06-21 03:22:29'),
(342, 'Indikator.News', 'script', '1.8.0', 'add_prefix_to_tables.php', '2021-06-21 03:22:29'),
(343, 'Indikator.News', 'comment', '1.8.0', 'Adding feature of logging newsletters.', '2021-06-21 03:22:29'),
(344, 'Indikator.News', 'script', '1.8.1', 'update_send_feature.php', '2021-06-21 03:22:29'),
(345, 'Indikator.News', 'comment', '1.8.1', 'Reworked sending newsletter and resend feature.', '2021-06-21 03:22:29'),
(346, 'Indikator.News', 'comment', '1.8.2', 'Minor improvements and bugfix.', '2021-06-21 03:22:29'),
(347, 'Indikator.News', 'comment', '1.8.3', 'Fixed the sending bug and added new settings.', '2021-06-21 03:22:29'),
(348, 'Indikator.News', 'comment', '1.8.4', 'Added the preview feature for posts.', '2021-06-21 03:22:29'),
(349, 'Indikator.News', 'comment', '1.8.5', 'Added more options for Settings page.', '2021-06-21 03:22:29'),
(350, 'Indikator.News', 'script', '1.8.6', 'update_send_feature_2.php', '2021-06-21 03:22:30'),
(351, 'Indikator.News', 'comment', '1.8.6', '!!! Reworked again the sending newsletter feature.', '2021-06-21 03:22:30'),
(352, 'Indikator.News', 'script', '1.9.0', 'create_categories_table.php', '2021-06-21 03:22:30'),
(353, 'Indikator.News', 'script', '1.9.0', 'add_category_field_to_table.php', '2021-06-21 03:22:30'),
(354, 'Indikator.News', 'comment', '1.9.0', 'Added categories feature.', '2021-06-21 03:22:30'),
(355, 'Indikator.News', 'comment', '1.9.1', 'Minor improvements and bugfix.', '2021-06-21 03:22:30'),
(356, 'Indikator.News', 'comment', '1.9.2', 'Added HTML variables to docs.', '2021-06-21 03:22:30'),
(357, 'Indikator.News', 'comment', '1.9.3', 'Added show only translated posts option.', '2021-06-21 03:22:30'),
(358, 'Indikator.News', 'comment', '1.9.4', 'Added Categories component and minor improvements.', '2021-06-21 03:22:30'),
(359, 'Indikator.News', 'script', '1.9.5', 'change_columns_type_2.php', '2021-06-21 03:22:30'),
(360, 'Indikator.News', 'comment', '1.9.5', 'Added Português translation.', '2021-06-21 03:22:30'),
(361, 'Indikator.News', 'comment', '1.9.5', 'Minor improvements and bugfix.', '2021-06-21 03:22:30'),
(362, 'Indikator.News', 'comment', '1.9.5', 'Change columns type and add index.', '2021-06-21 03:22:30'),
(363, 'Indikator.News', 'comment', '1.9.6', 'Added cloning feature.', '2021-06-21 03:22:30'),
(364, 'Indikator.News', 'script', '1.10.0', 'adding_gdpr_fields_for_subscribers.php', '2021-06-21 03:22:30'),
(365, 'Indikator.News', 'comment', '1.10.0', 'Plugin is now GDPR conform.', '2021-06-21 03:22:30'),
(366, 'Indikator.News', 'comment', '1.10.0', 'Added new option for Settings.', '2021-06-21 03:22:30'),
(367, 'Indikator.News', 'comment', '1.10.0', 'Minor UI improvements.', '2021-06-21 03:22:30'),
(368, 'Indikator.News', 'comment', '1.10.1', 'Improved the UI and widgets.', '2021-06-21 03:22:30'),
(369, 'Indikator.News', 'comment', '1.10.2', 'Added quick statistics for lists.', '2021-06-21 03:22:30'),
(370, 'Indikator.News', 'comment', '1.10.2', 'Updated the export / import.', '2021-06-21 03:22:30'),
(371, 'Indikator.News', 'comment', '1.10.2', 'Modernized the source code.', '2021-06-21 03:22:30'),
(372, 'Indikator.News', 'comment', '1.10.3', 'Minor UI improvements.', '2021-06-21 03:22:30'),
(373, 'Indikator.News', 'script', '1.10.4', 'adding_custom_newsletter_content_fields_to_posts.php', '2021-06-21 03:22:30'),
(374, 'Indikator.News', 'comment', '1.10.4', 'Adding custom newsletter content.', '2021-06-21 03:22:30'),
(375, 'Indikator.News', 'comment', '1.10.5', 'Minor UI and code improvements.', '2021-06-21 03:22:30'),
(376, 'Indikator.News', 'comment', '1.10.6', 'Improved the mail statistics of user.', '2021-06-21 03:22:30'),
(377, 'Indikator.News', 'script', '1.10.7', 'add_user_field_to_table.php', '2021-06-21 03:22:30'),
(378, 'Indikator.News', 'comment', '1.10.7', 'Added new options for Settings.', '2021-06-21 03:22:30'),
(379, 'Indikator.News', 'script', '1.10.8', 'add_tags_field_to_table.php', '2021-06-21 03:22:30'),
(380, 'Indikator.News', 'comment', '1.10.8', 'Added optional tags list for News.', '2021-06-21 03:22:30'),
(381, 'Indikator.News', 'comment', '1.10.8', 'Added new sorting options to component.', '2021-06-21 03:22:30'),
(382, 'Indikator.News', 'comment', '1.10.9', 'Minor improvements and bugfix.', '2021-06-21 03:22:30'),
(383, 'Indikator.News', 'comment', '1.11.0', 'Added SEO fields to news.', '2021-06-21 03:22:30'),
(384, 'Indikator.News', 'script', '1.11.1', 'add_seo_fields_to_table.php', '2021-06-21 03:22:30'),
(385, 'Indikator.News', 'comment', '1.11.1', 'Fixed the issue about new fields.', '2021-06-21 03:22:30'),
(386, 'Indikator.News', 'comment', '1.11.2', 'Added permission to Dashboard widgets.', '2021-06-21 03:22:30'),
(387, 'Indikator.News', 'comment', '1.11.3', 'Added additional fields that are copied when cloning a post.', '2021-06-21 03:22:30'),
(388, 'Indikator.News', 'comment', '1.11.4', 'Minor improvements and bugfixes.', '2021-06-21 03:22:30'),
(389, 'Indikator.News', 'script', '1.11.5', 'change_varchar_length.php', '2021-06-21 03:22:30'),
(390, 'Indikator.News', 'comment', '1.11.5', 'Minor UI and database improvements.', '2021-06-21 03:22:30'),
(391, 'Indikator.News', 'script', '1.11.6', 'common_rename_comment.php', '2021-06-21 03:22:30'),
(392, 'Indikator.News', 'comment', '1.11.6', 'Improved the list filters.', '2021-06-21 03:22:30'),
(393, 'Indikator.News', 'comment', '1.11.7', 'Improved the frontend components.', '2021-06-21 03:22:30'),
(394, 'Indikator.News', 'comment', '1.11.8', 'Fixed bug showing the previous and next post', '2021-06-21 03:22:30'),
(395, 'RainLab.Builder', 'comment', '1.0.1', 'Initialize plugin.', '2021-06-21 03:22:49');
INSERT INTO `system_plugin_history` (`id`, `code`, `type`, `version`, `detail`, `created_at`) VALUES
(396, 'RainLab.Builder', 'comment', '1.0.2', 'Fixes the problem with selecting a plugin. Minor localization corrections. Configuration files in the list and form behaviors are now autocomplete.', '2021-06-21 03:22:49'),
(397, 'RainLab.Builder', 'comment', '1.0.3', 'Improved handling of the enum data type.', '2021-06-21 03:22:49'),
(398, 'RainLab.Builder', 'comment', '1.0.4', 'Added user permissions to work with the Builder.', '2021-06-21 03:22:49'),
(399, 'RainLab.Builder', 'comment', '1.0.5', 'Fixed permissions registration.', '2021-06-21 03:22:49'),
(400, 'RainLab.Builder', 'comment', '1.0.6', 'Fixed front-end record ordering in the Record List component.', '2021-06-21 03:22:49'),
(401, 'RainLab.Builder', 'comment', '1.0.7', 'Builder settings are now protected with user permissions. The database table column list is scrollable now. Minor code cleanup.', '2021-06-21 03:22:49'),
(402, 'RainLab.Builder', 'comment', '1.0.8', 'Added the Reorder Controller behavior.', '2021-06-21 03:22:49'),
(403, 'RainLab.Builder', 'comment', '1.0.9', 'Minor API and UI updates.', '2021-06-21 03:22:49'),
(404, 'RainLab.Builder', 'comment', '1.0.10', 'Minor styling update.', '2021-06-21 03:22:49'),
(405, 'RainLab.Builder', 'comment', '1.0.11', 'Fixed a bug where clicking placeholder in a repeater would open Inspector. Fixed a problem with saving forms with repeaters in tabs. Minor style fix.', '2021-06-21 03:22:49'),
(406, 'RainLab.Builder', 'comment', '1.0.12', 'Added support for the Trigger property to the Media Finder widget configuration. Names of form fields and list columns definition files can now contain underscores.', '2021-06-21 03:22:49'),
(407, 'RainLab.Builder', 'comment', '1.0.13', 'Minor styling fix on the database editor.', '2021-06-21 03:22:49'),
(408, 'RainLab.Builder', 'comment', '1.0.14', 'Added support for published_at timestamp field', '2021-06-21 03:22:49'),
(409, 'RainLab.Builder', 'comment', '1.0.15', 'Fixed a bug where saving a localization string in Inspector could cause a JavaScript error. Added support for Timestamps and Soft Deleting for new models.', '2021-06-21 03:22:49'),
(410, 'RainLab.Builder', 'comment', '1.0.16', 'Fixed a bug when saving a form with the Repeater widget in a tab could create invalid fields in the form\'s outside area. Added a check that prevents creating localization strings inside other existing strings.', '2021-06-21 03:22:49'),
(411, 'RainLab.Builder', 'comment', '1.0.17', 'Added support Trigger attribute support for RecordFinder and Repeater form widgets.', '2021-06-21 03:22:49'),
(412, 'RainLab.Builder', 'comment', '1.0.18', 'Fixes a bug where \'::class\' notations in a model class definition could prevent the model from appearing in the Builder model list. Added emptyOption property support to the dropdown form control.', '2021-06-21 03:22:49'),
(413, 'RainLab.Builder', 'comment', '1.0.19', 'Added a feature allowing to add all database columns to a list definition. Added max length validation for database table and column names.', '2021-06-21 03:22:49'),
(414, 'RainLab.Builder', 'comment', '1.0.20', 'Fixes a bug where form the builder could trigger the \"current.hasAttribute is not a function\" error.', '2021-06-21 03:22:49'),
(415, 'RainLab.Builder', 'comment', '1.0.21', 'Back-end navigation sort order updated.', '2021-06-21 03:22:49'),
(416, 'RainLab.Builder', 'comment', '1.0.22', 'Added scopeValue property to the RecordList component.', '2021-06-21 03:22:49'),
(417, 'RainLab.Builder', 'comment', '1.0.23', 'Added support for balloon-selector field type, added Brazilian Portuguese translation, fixed some bugs', '2021-06-21 03:22:49'),
(418, 'RainLab.Builder', 'comment', '1.0.24', 'Added support for tag list field type, added read only toggle for fields. Prevent plugins from using reserved PHP keywords for class names and namespaces', '2021-06-21 03:22:49'),
(419, 'RainLab.Builder', 'comment', '1.0.25', 'Allow editing of migration code in the \"Migration\" popup when saving changes in the database editor.', '2021-06-21 03:22:49'),
(420, 'RainLab.Builder', 'comment', '1.0.26', 'Allow special default values for columns and added new \"Add ID column\" button to database editor.', '2021-06-21 03:22:49'),
(421, 'RainLab.Builder', 'comment', '1.0.27', 'Added ability to use \'scope\' in a form relation field, added ability to change the sort order of versions and added additional properties for repeater widget in form builder. Added Polish translation.', '2021-06-21 03:22:49'),
(422, 'RainLab.Builder', 'comment', '1.0.28', 'Fixes support for PHP 8', '2021-06-21 03:22:49'),
(423, 'RainLab.Builder', 'comment', '1.0.29', 'Disable touch device detection', '2021-06-21 03:22:49'),
(424, 'RainLab.Builder', 'comment', '1.0.30', 'Minor styling improvements', '2021-06-21 03:22:49'),
(425, 'RainLab.Editable', 'comment', '1.0.1', 'First version of Editable', '2021-06-21 03:23:08'),
(426, 'RainLab.Editable', 'comment', '1.0.2', 'Minor security update', '2021-06-21 03:23:08'),
(427, 'RainLab.Editable', 'comment', '1.0.3', 'Content save error fix', '2021-06-21 03:23:08'),
(428, 'RainLab.Editable', 'comment', '1.0.4', 'Fixed references to Redactor\'s \"get\" and \"destroy\" methods', '2021-06-21 03:23:08'),
(429, 'RainLab.Editable', 'comment', '1.0.5', 'Fixed button css issue', '2021-06-21 03:23:08'),
(430, 'RainLab.Editable', 'comment', '1.0.6', 'Remove dependency on backend rich editor control.', '2021-06-21 03:23:08'),
(431, 'RainLab.Editable', 'comment', '1.0.7', 'Fixed size() is not a function bug, granted access to users with `rainlab.pages.manage_content`, added Hungarian, Turkish, & Slovenian translations.', '2021-06-21 03:23:08');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_versions`
--

CREATE TABLE `system_plugin_versions` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `is_frozen` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_plugin_versions`
--

INSERT INTO `system_plugin_versions` (`id`, `code`, `version`, `created_at`, `is_disabled`, `is_frozen`) VALUES
(1, 'October.Demo', '1.0.1', '2021-06-21 02:56:41', 0, 0),
(2, 'RainLab.Pages', '1.4.5', '2021-06-21 03:16:04', 0, 0),
(3, 'JanVince.SmallContactForm', '1.52.1', '2021-06-21 03:19:48', 0, 0),
(4, 'RainLab.Translate', '1.9.1', '2021-06-21 03:22:07', 0, 0),
(5, 'Indikator.News', '1.11.8', '2021-06-21 03:22:30', 0, 0),
(6, 'RainLab.Builder', '1.0.30', '2021-06-21 03:22:49', 0, 0),
(7, 'RainLab.Editable', '1.0.7', '2021-06-21 03:23:08', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `system_request_logs`
--

CREATE TABLE `system_request_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `count` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_revisions`
--

CREATE TABLE `system_revisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revisionable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

CREATE TABLE `system_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_settings`
--

INSERT INTO `system_settings` (`id`, `item`, `value`) VALUES
(1, 'backend_brand_settings', '{\"app_name\":\"Damu Education\",\"app_tagline\":\"Getting back to basics\",\"primary_color\":\"#34495e\",\"secondary_color\":\"#e67e22\",\"accent_color\":\"#3498db\",\"menu_mode\":\"collapse\",\"custom_css\":\"\"}'),
(2, 'system_mail_settings', '{\"send_mode\":\"smtp\",\"sender_name\":\"OctoberCMS\",\"sender_email\":\"noreply@domain.tld\",\"sendmail_path\":\"\\/usr\\/sbin\\/sendmail -bs\",\"smtp_address\":\"smtp.mailgun.org\",\"smtp_port\":\"587\",\"smtp_user\":\"\",\"smtp_password\":\"\",\"smtp_authorization\":\"0\",\"smtp_encryption\":\"tls\",\"mailgun_domain\":\"\",\"mailgun_secret\":\"\",\"mandrill_secret\":\"\",\"ses_key\":\"\",\"ses_secret\":\"\",\"ses_region\":\"\",\"sparkpost_secret\":\"\"}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_users`
--
ALTER TABLE `backend_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login_unique` (`login`),
  ADD UNIQUE KEY `email_unique` (`email`),
  ADD KEY `act_code_index` (`activation_code`),
  ADD KEY `reset_code_index` (`reset_password_code`),
  ADD KEY `admin_role_index` (`role_id`);

--
-- Indexes for table `backend_users_groups`
--
ALTER TABLE `backend_users_groups`
  ADD PRIMARY KEY (`user_id`,`user_group_id`);

--
-- Indexes for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_unique` (`name`),
  ADD KEY `code_index` (`code`);

--
-- Indexes for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_item_index` (`user_id`,`namespace`,`group`,`item`);

--
-- Indexes for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_unique` (`name`),
  ADD KEY `role_code_index` (`code`);

--
-- Indexes for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `backend_user_throttle_user_id_index` (`user_id`),
  ADD KEY `backend_user_throttle_ip_address_index` (`ip_address`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_data_theme_index` (`theme`);

--
-- Indexes for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_logs_type_index` (`type`),
  ADD KEY `cms_theme_logs_theme_index` (`theme`),
  ADD KEY `cms_theme_logs_user_id_index` (`user_id`);

--
-- Indexes for table `cms_theme_templates`
--
ALTER TABLE `cms_theme_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_templates_source_index` (`source`),
  ADD KEY `cms_theme_templates_path_index` (`path`);

--
-- Indexes for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deferred_bindings_master_type_index` (`master_type`),
  ADD KEY `deferred_bindings_master_field_index` (`master_field`),
  ADD KEY `deferred_bindings_slave_type_index` (`slave_type`),
  ADD KEY `deferred_bindings_slave_id_index` (`slave_id`),
  ADD KEY `deferred_bindings_session_key_index` (`session_key`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `indikator_news_categories`
--
ALTER TABLE `indikator_news_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `indikator_news_categories_sort_order_index` (`sort_order`),
  ADD KEY `indikator_news_categories_slug_index` (`slug`);

--
-- Indexes for table `indikator_news_newsletter_logs`
--
ALTER TABLE `indikator_news_newsletter_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `indikator_news_posts`
--
ALTER TABLE `indikator_news_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `indikator_news_posts_category_id_index` (`category_id`),
  ADD KEY `indikator_news_posts_featured_index` (`featured`),
  ADD KEY `indikator_news_posts_published_at_index` (`published_at`),
  ADD KEY `indikator_news_posts_slug_index` (`slug`),
  ADD KEY `indikator_news_posts_user_id_index` (`user_id`);

--
-- Indexes for table `indikator_news_relations`
--
ALTER TABLE `indikator_news_relations`
  ADD PRIMARY KEY (`subscriber_id`,`categories_id`);

--
-- Indexes for table `indikator_news_subscribers`
--
ALTER TABLE `indikator_news_subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `janvince_smallcontactform_messages`
--
ALTER TABLE `janvince_smallcontactform_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `janvince_smallcontactform_messages_remote_ip_index` (`remote_ip`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rainlab_translate_attributes`
--
ALTER TABLE `rainlab_translate_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_attributes_locale_index` (`locale`),
  ADD KEY `rainlab_translate_attributes_model_id_index` (`model_id`),
  ADD KEY `rainlab_translate_attributes_model_type_index` (`model_type`);

--
-- Indexes for table `rainlab_translate_indexes`
--
ALTER TABLE `rainlab_translate_indexes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_indexes_locale_index` (`locale`),
  ADD KEY `rainlab_translate_indexes_model_id_index` (`model_id`),
  ADD KEY `rainlab_translate_indexes_model_type_index` (`model_type`),
  ADD KEY `rainlab_translate_indexes_item_index` (`item`);

--
-- Indexes for table `rainlab_translate_locales`
--
ALTER TABLE `rainlab_translate_locales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_locales_code_index` (`code`),
  ADD KEY `rainlab_translate_locales_name_index` (`name`);

--
-- Indexes for table `rainlab_translate_messages`
--
ALTER TABLE `rainlab_translate_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_messages_code_index` (`code`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_event_logs_level_index` (`level`);

--
-- Indexes for table `system_files`
--
ALTER TABLE `system_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_files_field_index` (`field`),
  ADD KEY `system_files_attachment_id_index` (`attachment_id`),
  ADD KEY `system_files_attachment_type_index` (`attachment_type`);

--
-- Indexes for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_mail_templates_layout_id_index` (`layout_id`);

--
-- Indexes for table `system_parameters`
--
ALTER TABLE `system_parameters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_index` (`namespace`,`group`,`item`);

--
-- Indexes for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_history_code_index` (`code`),
  ADD KEY `system_plugin_history_type_index` (`type`);

--
-- Indexes for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_versions_code_index` (`code`);

--
-- Indexes for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_revisions`
--
ALTER TABLE `system_revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`),
  ADD KEY `system_revisions_user_id_index` (`user_id`),
  ADD KEY `system_revisions_field_index` (`field`);

--
-- Indexes for table `system_settings`
--
ALTER TABLE `system_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_settings_item_index` (`item`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_users`
--
ALTER TABLE `backend_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_theme_templates`
--
ALTER TABLE `cms_theme_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `indikator_news_categories`
--
ALTER TABLE `indikator_news_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `indikator_news_newsletter_logs`
--
ALTER TABLE `indikator_news_newsletter_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `indikator_news_posts`
--
ALTER TABLE `indikator_news_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `indikator_news_subscribers`
--
ALTER TABLE `indikator_news_subscribers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `janvince_smallcontactform_messages`
--
ALTER TABLE `janvince_smallcontactform_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `rainlab_translate_attributes`
--
ALTER TABLE `rainlab_translate_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_translate_indexes`
--
ALTER TABLE `rainlab_translate_indexes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_translate_locales`
--
ALTER TABLE `rainlab_translate_locales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rainlab_translate_messages`
--
ALTER TABLE `rainlab_translate_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_files`
--
ALTER TABLE `system_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `system_parameters`
--
ALTER TABLE `system_parameters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=432;

--
-- AUTO_INCREMENT for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_revisions`
--
ALTER TABLE `system_revisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_settings`
--
ALTER TABLE `system_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
